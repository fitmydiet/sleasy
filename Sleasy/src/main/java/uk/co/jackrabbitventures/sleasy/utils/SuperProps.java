package uk.co.jackrabbitventures.sleasy.utils;


import java.io.*;
import java.util.Properties;

public class SuperProps extends Properties {

   private static final long serialVersionUID = 8330041952949510616L;
   private final String NAME_OF_DOT_DIRECTORY = ".sleasyprops";
   private final String MINUS_D_PARAM = "propsdir";
   private String directoryOffHomeForProps = System.getProperty("user.home") + File.separator + NAME_OF_DOT_DIRECTORY
           + File.separator + "props";
   private String directoryOffClasspathRoot = "props";
   private String propsFilename;

   /**
    * Pass in only the filename of the file you want to use.<br>
    * In development, put this file in your resources/props directory<br>
    * In development, if you want to use a file specific to your machine rather than to the project, put the file in the .sleasyprops directory in your home directory, this will override the default resources/props directory<br>
    * In the QA / UAT and other environments a java system parameter should be set that overrides this with a specified directory, SuperProps will look in there for the specified file<br>
    * <br>
    * <b>In Short:</b><br>
    * The resources/props location will be overridden by:<br>
    * The HOME_DIR/.sleasyprops location (if it exists) which will be overridden by:<br>
    * The propsdir System variable location (which should exist only in the deployed to environments).<br>
    * You always have to specify the filename from code.
    *
    * @param filename the name of the system prop you're wanting to use or the name of the file on your classpath for your local testing environemnt
    */
   public SuperProps(String filename) {

      this.loadProperties(filename);
   }

   private void loadProperties(String propsFilename) {

      this.propsFilename = propsFilename;

      InputStream propsInput;

      propsInput = getMinusDProps();

      if (propsInput == null) {
         propsInput = getExternalProps();
      }

      if (propsInput == null) {
         propsInput = getInternalProps();
      }

      if (propsInput == null) {
         throw new RuntimeException("Please put the " + propsFilename + " properties file into the "
                 + directoryOffHomeForProps + " directory \nYou may have to create the directories in the path. Or use the -D" + MINUS_D_PARAM + "  System variable to specify the path (do not include the filename)");
      }

      try {
         super.load(propsInput);
      } catch (IOException ldex) {
         throw new RuntimeException("Can't load properties data for some reason.", ldex);
      }

   }

   private InputStream getInternalProps() {

      String inJarOffClasspathRootPath = directoryOffClasspathRoot + File.separator + propsFilename;

      InputStream answer = SuperProps.class.getClassLoader().getResourceAsStream(inJarOffClasspathRootPath);

      return answer;

   }

   private InputStream getExternalProps() {

      InputStream answer = null;

      String externalHomeDirPath = directoryOffHomeForProps + File.separator + propsFilename;

      File extFile = new File(externalHomeDirPath);

      if (extFile.exists()) {

         try {
            answer = new FileInputStream(externalHomeDirPath);
         } catch (FileNotFoundException e) {
            throw new RuntimeException(
                    "Can't load properties data from the external directory " + externalHomeDirPath
                            + "for some reason.  Although the properties file has been found. Check the formatting.",
                    e);
         }
      }
      return answer;
   }

   private InputStream getMinusDProps() {

      InputStream answer = null;
      String minusDPathIncFile = null;
      String pathFromJavaParam = null;

      String paramVal = System.getProperty(MINUS_D_PARAM);
      if (null != paramVal && !paramVal.isEmpty()) {

         pathFromJavaParam = paramVal;

         minusDPathIncFile = pathFromJavaParam + File.separator + propsFilename;
         File javaParamFile = new File(minusDPathIncFile);
         if (javaParamFile.exists()) {

            try {
               answer = new FileInputStream(minusDPathIncFile);
            } catch (FileNotFoundException e) {
               throw new RuntimeException(
                       "Can't load properties file specified in the system -D parameter passed to the JVM:  -D"
                               + pathFromJavaParam
                               + "for some reason.  Although the properties file has been found. Check the formatting.", e);

            }

         }

      }

      return answer;

   }

   public String getProperty(String key) {

      String answer = super.getProperty(key);

      //TODO: This isn't convention over configuration. Consider changing this and having all possible properties documented in a different way.
      if (answer == null) {
         throw new RuntimeException(
                 "The property key: " + key + " does not exist in the properties file " + propsFilename + ".  It's fine for the property's value not to be there, but SuperProps insists that the property its self exists");
      }

      return answer;

   }

}
