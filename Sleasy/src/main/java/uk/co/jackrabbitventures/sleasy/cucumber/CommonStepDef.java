package uk.co.jackrabbitventures.sleasy.cucumber;


import org.apache.commons.lang3.time.DateUtils;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import org.openqa.selenium.ie.InternetExplorerDriver;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.jackrabbitventures.sleasy.dataset.DatabaseRestorer;
import uk.co.jackrabbitventures.sleasy.utils.SuperProps;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;


/**
 * This class allows a static instance of WebDriver to be used across multiple
 * step definition classes.
 *
 * @author RM Hind
 */

public abstract class CommonStepDef {

    static final Logger log = LoggerFactory.getLogger(CommonStepDef.class);

    protected SuperProps superProps; //Set in subclass so every sleasy app can have a different props file

    static boolean isRun = false;

    // static WebDriver - essential for being able to share one instance among all of the concrete stepdefs
    protected static WebDriver driver;

    // TODO: Check scope on here, default scope? Really? Consider protected if we're going to override, or private if not.
    Connection jdbcConnection;
    private DatabaseRestorer dataRestorer;
    protected boolean dbOn = false;


    /**
     * When this is called by the framework we can guarantee that the protected 'driver' variable is fully initialised and available.<br>
     * This means anything that depends on it (helpers) can be set up here.
     */
    protected abstract void initHelpers();


    protected void beforeHook(String setupFile) {


        if ("true".equals(superProps.getProperty("database.setupenabled"))) {

            dbOn = true;
            setUpDatabase(setupFile);
        }

        getDriver();
        initHelpers();

    }

    protected void afterHook() {
        // Close the browser and nullify the driver

        if (dbOn) {
            try {
                jdbcConnection.close();
            } catch (SQLException e) {
                throw new RuntimeException("There was a problem closing the database connection after one of the scenarios ", e);
            }
        }
        driver.close();
        //driver.quit();
        driver = null;
    }

    private void setUpDatabase(String datafile) {
        // database connection

        IDatabaseConnection dbUnitConnection;
        try {
            Class.forName(superProps.getProperty("database.drivername"));
            jdbcConnection = DriverManager.getConnection(superProps.getProperty("database.connectionstring"), superProps.getProperty("database.username"), superProps.getProperty("database.pword"));
            dbUnitConnection = new DatabaseConnection(jdbcConnection, superProps.getProperty("database.schema"));
        } catch (Exception e) {
            throw new RuntimeException("There was a problem with the database setup in file " + datafile + " Are you sure you want data setup (database.setupenabled) to be enabled in sleasy.props ?", e);
        }

        try {
            // initialize your dataset here
            FlatXmlDataSetBuilder loader = new FlatXmlDataSetBuilder();
            URL url = this.getClass().getClassLoader().getResource(datafile);
            IDataSet ds = loader.build(url);

            ReplacementDataSet rDataSet = new ReplacementDataSet(ds);
            replaceXMLPlaceholders(rDataSet);

            DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnection, rDataSet);

        } catch (Exception e) {
            throw new RuntimeException("There was a problem with the database setup in file " + datafile, e);
        } finally {
            try {
                dbUnitConnection.close();
                jdbcConnection.close();
            } catch (SQLException sqlEx) {
                throw new RuntimeException("There was a problem closing the database in file " + datafile, sqlEx);
            }
        }
    }

    protected WebDriver getDriver() {

        if (driver == null) {

            String toUse = superProps.getProperty("browser.touse");

            log.info("Trying to use the following driver: {}", toUse);

            if ("phantom".equalsIgnoreCase(toUse) || "phantomjs".equalsIgnoreCase(toUse) || "ph".equalsIgnoreCase(toUse)) {
/*
                DesiredCapabilities desCaps = new DesiredCapabilities();
                desCaps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, superProps.getProperty("browser.pathto.phantom"));
                driver = new PhantomJSDriver(desCaps);
*/
            } else if ("iedriver".equalsIgnoreCase(toUse) || "ie".equalsIgnoreCase(toUse) || "iexplore".equalsIgnoreCase(toUse) || "internetexplorer".equalsIgnoreCase(toUse) || "explorer".equalsIgnoreCase(toUse)) {

                File file = new File(superProps.getProperty("browser.pathto.ie"));
                System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
                driver = new InternetExplorerDriver();

            } else if ("firefox".equalsIgnoreCase(toUse) || "ff".equalsIgnoreCase(toUse)) {


                log.info("ff 1");

                String pathToFirefox = superProps.getProperty("browser.pathto.firefox");
                log.info("ff 2");
                String pathToGecko = superProps.getProperty("browser.pathto.gecko");
                log.info("ff 3");
                System.setProperty("webdriver.firefox.bin", pathToFirefox);
                log.info("ff 4");
                System.setProperty("webdriver.gecko.driver", pathToGecko);
                log.info("ff 5");
                System.setProperty("webdriver.firefox.marionette", "false");
                log.info("ff 6");

                FirefoxOptions options = new FirefoxOptions();
              //  options.setBinary(pathToFirefox); //This is the location where you have installed Firefox on your machine

                driver = new FirefoxDriver(options);
                //  driver.get("http://www.google.com");


                log.info("ff 7");
              //  driver.get("http://www.fitmydiet.com");
                log.info("ff 8");

            } else if ("chrome".equalsIgnoreCase(toUse) || "chromedriver".equalsIgnoreCase(toUse)) {

            /*
            String pathToChrome = superProps.getProperty("browser.pathto.chromedriver");
            System.setProperty("webdriver.chrome.driver", pathToChrome);

            HashMap<String, Object> chromePrefs = new HashMap<>();
            chromePrefs.put("profile.default_content_settings.popups", 0);
            chromePrefs.put("download.default_directory", getDownloadFolderPath());

            ChromeOptions options = new ChromeOptions();
            //options.addArguments("no-sandbox");
            options.setExperimentalOption("prefs", chromePrefs);



            DesiredCapabilities cap = DesiredCapabilities.chrome();
            cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            cap.setCapability(ChromeOptions.CAPABILITY, options);



            driver = new ChromeDriver(options);

*/

            } else {
                /*
                driver = new HtmlUnitDriver(true);

                 */
            }

        }

        return driver;
    }


    public static String getDownloadFolderPath() {

        //setting the specific download location so we can check files successfully download
        StringBuilder folder = new StringBuilder();
        folder.append(System.getProperty("user.dir"));
        folder.append(File.separator);
        folder.append("src");
        folder.append(File.separator);
        folder.append("test");
        folder.append(File.separator);
        folder.append("resources");
        folder.append(File.separator);
        folder.append("downloads");

        //check to see if the folder exists. if it does, remove any files from inside it
        // else make the directory, if it's not there
        File f = new File(folder.toString());
        if (f.exists()) {
            log.debug("The download directory: " + f.toPath().toString() + " already exists");

            for (File file : f.listFiles()) {
                log.debug("Deleting file = " + file.getName());
                file.delete();
            }
        } else {
            log.debug("Creating directory " + f.toPath().toString());
            f.mkdir();
        }

        //finish off the path
        folder.append(File.separator);

        log.debug("Downloads folder is being set to: " + folder);

        return folder.toString();
    }

    private void runBeforeAll() {

        if (dbOn) {
            dataRestorer.backupData();
        }
    }

    private void runAfterAll() {

        if (dbOn) {
            dataRestorer.restoreDb();
        }
    }

    protected void beforeAll(DatabaseRestorer dataRestorer) {

        if (dbOn) {
            this.dataRestorer = dataRestorer;

            if (!isRun) {
                Runtime.getRuntime().addShutdownHook(new Thread() {
                    public void run() {

                        runAfterAll();
                    }
                });
                runBeforeAll();
                isRun = true;
            }
        }
    }

    protected void navigateTo(String absOrRelUrl) {

        if (absOrRelUrl.startsWith("http")) {

            getDriver().get(absOrRelUrl);

        } else {
            getDriver().get(superProps.getProperty("environment.baseurl") + absOrRelUrl);

        }
    }

    /**
     * Replaces predefined placeholders in the xml with values
     *
     * @param rDataSet
     */
    protected void replaceXMLPlaceholders(ReplacementDataSet rDataSet) {

        rDataSet.addReplacementObject("[date_last_week]", DateUtils.addDays(new Date(), -7));
        rDataSet.addReplacementObject("[date_yesterday]", DateUtils.addDays(new Date(), -1));
        rDataSet.addReplacementObject("[date_today]", new Date());
        rDataSet.addReplacementObject("[date_tomorrow]", DateUtils.addDays(new Date(), +1));
        rDataSet.addReplacementObject("[date_next_week]", DateUtils.addDays(new Date(), +7));
    }

}