package uk.co.jackrabbitventures.sleasy.dataset;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.DatabaseSequenceFilter;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.FilteredDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import uk.co.jackrabbitventures.sleasy.utils.SuperProps;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This is a tool of convenience to generate a dbUnit compatible xml dataset
 * from an existing database and then reload upon completion.
 */
public abstract class DatabaseRestorer {


    private static SuperProps superProps = new SuperProps("bddtestconfig.props");

    protected String FILENAME = "LatestPssDatasetBackup.xml";

    protected Connection jdbcConnection;
    protected IDatabaseConnection dbUnitConnection;
    protected Statement st = null;


    /**
     * May need to have
     * @throws SQLException
     */
    protected abstract void beforeBackup() throws SQLException;
    protected abstract void afterBackup() throws SQLException;
    protected abstract void beforeRestore() throws SQLException;
    protected abstract void afterRestore() throws SQLException;

    private void initConnection() {

        try {
            Class.forName(superProps.getProperty("database.drivername"));
            jdbcConnection = DriverManager.getConnection(superProps.getProperty("database.connectionstring"), superProps.getProperty("database.username"), superProps.getProperty("database.pword"));
            dbUnitConnection = new DatabaseConnection(jdbcConnection, superProps.getProperty("database.schema"));
        } catch (Exception e) {
            throw new RuntimeException("There was a problem with the database setup in file ", e);
        }
    }

    private void closeConnection() {
        try {
            if (st != null) {
                st.close();
                st = null;
            }
            dbUnitConnection.close();
            jdbcConnection.close();
            dbUnitConnection = null;
            jdbcConnection = null;

        } catch (SQLException sqlEx) {
            throw new RuntimeException("There was a problem closing the database in file ", sqlEx);
        }

    }

    private void doFullDbExport() {
        // full database export
        IDataSet fullDataSet = null; // This does in order of the keys
        try {

            fullDataSet = new FilteredDataSet(new DatabaseSequenceFilter(dbUnitConnection), dbUnitConnection.createDataSet());
            FlatXmlDataSet.write(fullDataSet, new FileOutputStream(System.getProperty("user.dir") + File.separator + FILENAME));

        } catch (DataSetException | SQLException | IOException e) {
            throw new RuntimeException("Exception when trying to do full Db Export ", e);
        }


    }

    public void backupData() {
        initConnection();

        try {
            //temporarily drop constraint in one roc table
            st = jdbcConnection.createStatement();

            beforeBackup();
            doFullDbExport();
            afterBackup();


        } catch (Exception e) {
            throw new RuntimeException("Exception when trying to extract the dataset ", e);
        } finally {
            closeConnection();
        }
    }

    public void restoreDb() {

        initConnection();

        try {

            st = jdbcConnection.createStatement();

            beforeRestore();
            fullWriteToDatabase();
            afterRestore();

        } catch (Exception e) {
            throw new RuntimeException("There was a problem with inserting data back into the database", e);
        } finally {
            closeConnection();
        }
    }

    private void fullWriteToDatabase() {

        // initialize your dataset here
        FlatXmlDataSetBuilder loader = new FlatXmlDataSetBuilder();
        IDataSet ds = null;
        try {
            ds = loader.build(new File(System.getProperty("user.dir") + File.separator + FILENAME));
            DatabaseOperation.CLEAN_INSERT.execute(dbUnitConnection, ds);
        } catch (DatabaseUnitException | SQLException | IOException e) {
            throw new RuntimeException("There was a problem writing the fully backed up dataset back to the database ", e);
        }

    }

}