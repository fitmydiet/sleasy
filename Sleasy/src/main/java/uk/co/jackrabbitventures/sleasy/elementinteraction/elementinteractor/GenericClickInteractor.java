package uk.co.jackrabbitventures.sleasy.elementinteraction.elementinteractor;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.jackrabbitventures.sleasy.elementinteraction.SleasyFieldHelper;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.fail;


public abstract class GenericClickInteractor extends SleasyFieldHelper {

   static final Logger log = LoggerFactory.getLogger(GenericClickInteractor.class);

   protected WebDriver driver;

   private String lastXpathUsed = "";

   private int ordinal = 1;

   private XPathCheckType checkTypeOverride = null;

   private Actions actions = null;

   protected Wait wait = null;

   protected int tout;

   protected String screenshotPath; // set in subclass
   private int screenshotNumber;

   public GenericClickInteractor(WebDriver driver) {
      super(driver);
     // this.driver = driver;
     // this.actions = new Actions(driver);

   }

   public enum XPathCheckType {
      VISIBLE, PRESENT, CLICKABLE, INVISIBLE, ALL_VISIBLE
   }

   /**
    * In the standard pss layout, this will find the label tag that surrounds the visible element matching the passed parameter.
    *
    * @param textInLabeledField
    * @return
    */
   private WebElement findLabelMatching(String textInLabeledField) {

      WebElement matchingElement = null;

      String xPathStmtExact = "//label[normalize-space(text())=normalize-space('" + textInLabeledField + "')]";
      String xPathStmtFuzzy = "//label[contains(., '" + textInLabeledField + "')]";

      matchingElement = tryXpath(xPathStmtExact, xPathStmtFuzzy);

      return matchingElement;

   }


   /**
    * This will find element which has the attached label description
    *
    * @param labelDescription
    * @return WebElement
    */
   protected WebElement findWebElementWithLabelMatching(String labelDescription) {

      WebElement labelElement = findLabelMatching(labelDescription);

      String idToMatch = labelElement.getAttribute("for");

      WebElement webElement = driver.findElement(By.id(idToMatch));
      return webElement;
   }


   /**
    * Finds an element by xpath.  If the xpath provided matches more than one element the element with the shortest string length is returned.
    *
    * @param xPaths
    * @return
    */
   protected WebElement tryXpath(String... xPaths) {

      return tryXpath(XPathCheckType.PRESENT, xPaths);
   }


   /**
    * Finds an element by xpath.  If the xpath provided matches more than one element the element with the shortest string length is returned.
    *
    * @return
    */
   protected WebElement tryXpath(XPathCheckType checkTypeToWaitFor, String... xPaths) {


      WebElement toReturn = null;

      // If ordinal is set, use it

      for (int i = 0; i < xPaths.length; i++) {
         if (ordinal > 1) {
            xPaths[i] = "(" + xPaths[i] + ")[" + ordinal + "]";
         }
      }

      if (checkTypeOverride != null) {
         checkTypeToWaitFor = checkTypeOverride;
      }


      String fullOrXpath = Stream.of(xPaths).collect(Collectors.joining(" | "));

      lastXpathUsed = fullOrXpath;

      if (checkTypeToWaitFor != null) {

         try {
            if (checkTypeToWaitFor.equals(XPathCheckType.VISIBLE)) {
               wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(fullOrXpath)));
            } else if (checkTypeToWaitFor.equals(XPathCheckType.PRESENT)) {
               wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(fullOrXpath)));
            } else if (checkTypeToWaitFor.equals(XPathCheckType.CLICKABLE)) {
               wait.until(ExpectedConditions.elementToBeClickable(By.xpath(fullOrXpath)));
            } else if (checkTypeToWaitFor.equals(XPathCheckType.ALL_VISIBLE)) {
               wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(fullOrXpath)));
            } else if (checkTypeToWaitFor.equals(XPathCheckType.INVISIBLE)) {
               wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(fullOrXpath)));
            }
         } catch (TimeoutException te) {
            String err = "We have waited for the xpath '" + fullOrXpath + "' to correspond to a " + checkTypeToWaitFor.name() + " element on the page for " + this.tout + " seconds. It didn't. This quality gate has therefore failed. Check the case.  This test is case specific.";
            failWithMessage(err);
         }

      }

      List<WebElement> matchingEls = driver.findElements(By.xpath(fullOrXpath));
      toReturn = getSmallestMatch(matchingEls);

      ordinal = 1;
      checkTypeOverride = null;
      return toReturn;
   }


   private WebElement getSmallestMatch(List<WebElement> matchingEls) {

      WebElement toReturn = null;

      if (matchingEls != null && !matchingEls.isEmpty()) {
         // Sort if there is more than one match
         if (matchingEls.size() > 1) {
            Collections.sort(matchingEls, (Comparator<? super WebElement>) new MatchedFieldComparator());
         }
         toReturn = matchingEls.get(0);
      } else {
         toReturn = null;
      }

      return toReturn;

   }


   class MatchedFieldComparator implements Comparator<WebElement> {

      public int compare(WebElement a, WebElement b) {

         return b.getText().length() - a.getText().length();
      }
   }


   public void setOrdinalForNext(int ordinal) {

      this.ordinal = ordinal;
   }


   public void setCheckTypeOverride(XPathCheckType waitFor) {

      this.checkTypeOverride = waitFor;
   }

   /**
    * @return absolute path to the screenshot taken
    */
   private String takeScreenshot() {


      String answer;
      Date now = new Date();
      int namingTieBraker = 0;
      String pathDate = new SimpleDateFormat("yyyyMMdd").format(now);
      String fileTime = new SimpleDateFormat("HH:mm.ss").format(now);
      String outFilePath = this.screenshotPath + File.separator + pathDate + File.separator + "screens" + File.separator;

      String title = driver.getTitle();
      if (title.isEmpty()) {
         title = "Sleasy: Page Has No Title";
      }
      File outFile = new File(outFilePath + fileTime + "_" + title + ".png");


      // if it exists change the name slightly using the namingTieBreaker until it doesn't
      DecimalFormat df;
      while (outFile.exists()) {
         df = new DecimalFormat("00"); // very rare that we will loop at all if ever, so creation in here rather than outside of the loop is correct!
         outFile = new File(outFilePath + fileTime + "(" + df.format(++namingTieBraker) + ")_" + title + ".png");
      }
      answer = outFile.getAbsolutePath();
      try {
         FileUtils.copyFile(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE), outFile);
         log.debug("Screenshot taken for " + title + " screen");
      } catch (IOException e) {
         answer = "Error taking screenshot and attempting to copy here: " + answer;
         log.error("Error taking screenshot " + outFile.getAbsolutePath(), e);
      }

      return answer;
   }


   /**
    * Returns true only if next exists AND It is enabled for clicking on. The word to look for on the next button is passed as a parameter
    *
    * @param wordOnButton
    * @return
    */
   public void buttonIsEnabled(String wordOnButton) {

      String xpathForNextButton = "//button[not(contains(@disabled,'disabled')) and contains(., '" + wordOnButton + "')]";
      String xpathForNextLinkButton = "//a[not(contains(@disabled,'disabled')) and contains(., '" + wordOnButton + "')]";

      tryXpath(XPathCheckType.CLICKABLE, xpathForNextButton, xpathForNextLinkButton);
   }


   /**
    * Returns true only if the button exists AND It is DISABLED. The word to look for on the next button is passed as a parameter
    */
   public void buttonIsDisabled() {

      buttonIsDisabled("Next");
   }

   /**
    * Returns true only if the button exists AND It is DISABLED. The word to look for on the next button is passed as a parameter
    *
    * @param wordOnButton
    */
   public void buttonIsDisabled(String wordOnButton) {

      String xpathForNextButton = "//button[contains(@disabled,'disabled') and contains(., '" + wordOnButton + "')]";
      tryXpath(XPathCheckType.VISIBLE, xpathForNextButton);
   }

   /**
    *
    */
   public void clickOnButton() {

      clickOnButton("Next");
   }

   /**
    * Click on a button with the name specified
    *
    * @param wordOnButton
    */
   public void clickOnButton(String wordOnButton) {

      WebElement button = null;
      String xpathUsualButton = "//input[@type='submit' and contains(@value,'" + wordOnButton + "')]";
      String xpathForNextButton = "//div[@role='navigation']//button[contains(@type,'submit') and contains(., '" + wordOnButton + "')] ";
      String xpathForLinButton = "//a[contains(@role,'button')and contains(., '" + wordOnButton + "')]";
      String xpathForLinkButtonWld = "//*[contains(@role, 'button') and contains(., '" + wordOnButton + "')]";
      String xpathForLinkButton = "//button[contains(@class,'button')and contains(., '" + wordOnButton + "')]";
      String xpathForLinkClassButton = "//a[contains(@class,'button')and contains(., '" + wordOnButton + "')]";
      String xpathForButton = "//button[contains(., '" + wordOnButton + "')]";
      button = tryXpath(XPathCheckType.CLICKABLE, xpathUsualButton, xpathForLinkButton, xpathForButton, xpathForLinButton, xpathForLinkButtonWld, xpathForNextButton, xpathForLinkClassButton);
      doClick(button);
   }


   public void clickOnSubmit() {

      WebElement subBut = null;
      String sub = "//input[contains(@type,'submit')]";
      subBut = tryXpath(XPathCheckType.CLICKABLE, sub);
      doClick(subBut);
   }


   private void ddSelection(WebElement we, String selectionToMake) {


      Select select = new Select(we);

      try {
         select.selectByValue(selectionToMake);
      } catch (NoSuchElementException nse1) {
         try {
            select.selectByVisibleText(selectionToMake);
         } catch (NoSuchElementException nse2) {
            String err = "Could not find selection " + selectionToMake + " ";
            failWithMessage(err);
         }
      }
   }

   public void selectFromDropdown(String textInLabeledField, String selectionToMake) {

      WebElement inputEl = findWebElementWithLabelMatching(textInLabeledField);
      ddSelection(inputEl, selectionToMake);
   }

   /**
    * To be used with set ordinal for next
    *
    * @param selectionToMake
    */
   public void selectFromDropdown(String selectionToMake) {

      final String checkboxXpath = "//select";
      WebElement inputEl = tryXpath(XPathCheckType.CLICKABLE, checkboxXpath);

      ddSelection(inputEl, selectionToMake);

   }


   public void fillIn(String textInLabeledField, String textToEnterIntoInput) {

      WebElement label = findLabelMatching(textInLabeledField);
      String idToMatchWithInput = label.getAttribute("for");
      WebElement inputEl = driver.findElement(By.id(idToMatchWithInput));
      inputEl.sendKeys(textToEnterIntoInput);

   }

   public WebElement getRadioButton(String radioGroupDescription, String selectionToMake) {

      final String radioLegendLabelXpath = "//*[contains(@class,'legend-label-bold') and contains(text(), '" + radioGroupDescription + "')]/../label[contains(., '" + selectionToMake + "')]/input[@type='radio']";
      final String radioXpath = "//*[contains(@class,'radio-label') and contains(text(), '" + radioGroupDescription + "')]/../label[contains(., '" + selectionToMake + "')]/input[@type='radio']";
      WebElement radioInput = tryXpath(XPathCheckType.PRESENT, radioXpath, radioLegendLabelXpath);

      return radioInput;
   }


   public void selectRadio(String radioGroupDescription, String selectionToMake) {

      WebElement radioInput = getRadioButton(radioGroupDescription, selectionToMake);
      doClick(radioInput);
   }

   public void radioButtonIsSelected(String radioGroupDescription) {

      final String radioLegendLabelXpath = "//*[contains(@class,'legend-label-bold') and contains(text(), '" + radioGroupDescription + "')]/../label[contains(@class,'selected')]/input[@type='radio']";
      final String radioXpath = "//*[contains(@class,'radio-label') and contains(text(), '" + radioGroupDescription + "')]/../label[contains(@class,'selected')]/input[@type='radio']";

      tryXpath(XPathCheckType.PRESENT, radioLegendLabelXpath, radioXpath);
   }

   public void radioButtonIsSelected(String radioGroupDescription, String selectionToMake) {

      final String radioLegendLabelXpath = "//*[contains(@class,'legend-label-bold') and contains(text(), '" + radioGroupDescription + "')]/../label[contains(., '" + selectionToMake + "') and contains(@class,'selected')]/input[@type='radio']";
      final String radioXpath = "//*[contains(@class,'radio-label') and contains(text(), '" + radioGroupDescription + "')]/../label[contains(., '" + selectionToMake + "') and contains(@class,'selected')]/input[@type='radio']";

      tryXpath(XPathCheckType.PRESENT, radioLegendLabelXpath, radioXpath);
   }

   public void noRadioButtonSelected(String radioGroupDescription) {

      final String radioLegendLabelXpath = "//*[contains(@class,'legend-label-bold') and contains(text(), '" + radioGroupDescription + "')]/../label[contains(@class,'selected')]/input[@type='radio']";
      final String radioXpath = "//*[contains(@class,'radio-label') and contains(text(), '" + radioGroupDescription + "')]/../label[contains(@class,'selected')]/input[@type='radio']";

      WebElement elThere = tryXpath(radioLegendLabelXpath, radioXpath);

      if (elThere != null) {
         tryXpath(XPathCheckType.INVISIBLE, radioLegendLabelXpath, radioXpath);
      }
   }

   public void clickOnLink(String linkText) {

      String linkXpath;

      if (linkText.contains("'")) {
         linkXpath = "//a[contains(., \"" + linkText + "\")]";
      } else {
         linkXpath = "//a[contains(., '" + linkText + "')]";
      }

      WebElement link = tryXpath(XPathCheckType.CLICKABLE, linkXpath);
      doClick(link);
   }

   /**
    * Affects a check of a checkbox that's description matches the passed in string.
    *
    * @param selectionToMake
    */
   public void selectCheckbox(String selectionToMake) {

      final String checkboxXpath = "//label[contains(.,'" + selectionToMake + "')]/input[@type='checkbox']";
      WebElement checkboxInput = tryXpath(XPathCheckType.CLICKABLE, checkboxXpath);
      doClick(checkboxInput);
   }

   /**
    * Affects a check of a checkbox. For use with setOrdinalForNext
    */
   public void selectCheckbox() {

      final String checkboxXpath = "//input[@type='checkbox']";
      WebElement checkboxInput = tryXpath(XPathCheckType.CLICKABLE, checkboxXpath);
      doClick(checkboxInput);
   }

   /**
    * Returns true only if the textInTag is displayed somewhere on the document. This does not wait for the text to exist.
    *
    * @param textInTag
    * @return
    */
   public void textExists(String textInTag) {

      // note that ordinals won't work with these wild cards.

      // consider raising an exception
      textExists(textInTag, "*");
   }

   public void textIsAbsent(String textInTag) {

      textIsAbsent(textInTag, "*");
   }

   /**
    * Returns true only if the textInTag is displayed on the document within the tag passed. This will wait for text to exist. This will fail if the text is not found.
    * WARNING: Do not use in conjunction with a not (!) if you do not expect the text to be there, rather use textIsAbsent
    *
    * @param textInTag
    * @param inTag
    * @return
    */
   public void textExists(String textInTag, String inTag) {

      final String xpath = "(//" + inTag + "[contains(., '" + textInTag + "')])[last()]";

      tryXpath(XPathCheckType.VISIBLE, xpath);
   }

   /**
    * Searches the specified field for a given value. Test fails if the field cannot be found, or if the passed value does not exist within that field.
    */
   public void textExistsInField(String valueToFind, String fieldName) {

      WebElement fieldElement = this.findWebElementWithLabelMatching(fieldName);
      try {
         this.wait.until(ExpectedConditions.attributeToBe(fieldElement, "value", valueToFind));
      } catch (TimeoutException te) {
         String err = "We have waited for the text '" + valueToFind + "' to appear in the field '" + fieldName + "' for " + tout + " seconds. It didn't. This quality gate has therefore failed. Check the case.  This test is case specific.";
         failWithMessage(err);
      }
   }

   /**
    * Searches for the given error message above the specified field.
    */
   public void errorExistsInField(String fieldName, String errorText) {

      WebElement fieldElement = this.findWebElementWithLabelMatching(fieldName);

      try {
         this.wait.until(ExpectedConditions.visibilityOfNestedElementsLocatedBy(fieldElement, By.xpath("//span[contains(@class,'error-message')and contains(., '" + errorText + "')]")));
      } catch (TimeoutException te) {
         String err = "We have waited for the error message '" + errorText + "' to appear above the field '" + fieldName + "' for " + tout + " seconds. It didn't. This quality gate has therefore failed. Check the case.  This test is case specific.";
         failWithMessage(err);
      }
   }

   /**
    * Returns true only if the textInTag is displayed on the document within the tag passed. This will wait for text to exist. This will fail if the text is not found.
    * WARNING: Do not use in conjunction with a not (!) if you do not expect the text to be there, rather use textDoesntExist
    *
    * @param textInTag
    * @param inTag
    * @return
    */
   public void textIsAbsent(String textInTag, String inTag) {

      final String xpath = "(//" + inTag + "[contains(., '" + textInTag + "')])[last()]";

      WebElement elThere = tryXpath(xpath);

      if (elThere != null) {
         tryXpath(XPathCheckType.INVISIBLE, xpath);
      }
   }


   /**
    * Fills in a blank value to the element which has the attached label description
    *
    * @param labelDescription
    */
   public void fillInBlank(String labelDescription) {

      WebElement webElement = findWebElementWithLabelMatching(labelDescription);
      webElement.clear();
   }

   /**
    * To be used in combination with setOrdinal Fo
    *
    * @param textToEnterIntoInput
    */
   public void fillIn(String textToEnterIntoInput) {


      final String validInput = "//input [not(contains(@disabled,'disabled'))]";

      WebElement elThere = tryXpath(XPathCheckType.VISIBLE, validInput);

      elThere.sendKeys(textToEnterIntoInput);

   }


   /**
    * Returns true if the element which has the attached label description contains no text
    *
    * @param labelDescription
    */
   public void isEmpty(String labelDescription) {

      WebElement webElement = findWebElementWithLabelMatching(labelDescription);

      try {
         wait.until(ExpectedConditions.or(ExpectedConditions.attributeToBe(webElement, "value", ""),
                 ExpectedConditions.textToBePresentInElement(webElement, "")));
      } catch (TimeoutException te) {
         String err = "The field " + labelDescription + " contains text when it should be empty.";
         failWithMessage(err);
      }

   }


   /**
    * Returns true if the element which has the attached label description contains any text
    *
    * @param labelDescription
    */
   public void isNotEmpty(String labelDescription) {

      WebElement webElement = findWebElementWithLabelMatching(labelDescription);

      try {
         wait.until(ExpectedConditions.or(ExpectedConditions.not(ExpectedConditions.attributeToBe(webElement, "value", "")),
                 ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(webElement, ""))));
      } catch (TimeoutException te) {
         String err = "The field " + labelDescription + " appears to be empty when it shouldn't be.";
         failWithMessage(err);
      }

   }


   /**
    * Returns true if the element which has the attached label description is highlighted as an error
    *
    * @param labelDescription
    */
   public void isHighlighted(String labelDescription) {

      WebElement webElement = findWebElementWithLabelMatching(labelDescription);

      try {
         wait.until(ExpectedConditions.attributeContains(webElement, "class", "ng-invalid"));
      } catch (TimeoutException te) {
         String err = "The field " + labelDescription + " is not highlighted.";
         failWithMessage(err);
      }
   }

   public void tabOnFromField(String fieldName) {

      WebElement fieldElement = this.findWebElementWithLabelMatching(fieldName);
      fieldElement.sendKeys(Keys.TAB);
   }

   /**
    * @param err
    */
   protected void failWithMessage(String err) {

      err = err + "\n\nScreenshot taken, accessible at:\n" + takeScreenshot();
      log.debug(err);
      fail(err);
   }

   /**
    * Performs a click action on the specified element.
    *
    * @param element
    */
   private void doClick(WebElement element) {

      actions.moveToElement(element).click().perform();
   }
}