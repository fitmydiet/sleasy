Feature: As a member of the pulbic, I would like to be able to sign an online petition so
  that I can express my views about a subject to the government of the day (and to demonstrate Sleasy ;-)

  Background:
    Given I am on the government petition website home page


  @NoDataNecessary
  Scenario: 1: REQ_STYLE: I can do a search and the Sign Petition button gets presented (Requirements style). Opens up the possibility for an agile BDD process.
    When I enter valid search criteria into the search box
    And a search listing appears
    Then I can sign a petition

  @NoDataNecessary
  Scenario: 2: TST_STYLE_PASS: I can do a search and the Sign Petition button gets presented - Tester style (low level) - Should PASS
    When I enter dog into the earch field
    Then I submit the form
    And Stop vets euthanising healthy dogs when options are available to is displayed on the page
    When I click on the Stop vets euthanising healthy dogs when options are available to link
    Then match keeper with presenter and exhaust all options available to save them by contacting rescues or breeders who is displayed on the page
    Then I click on the Sign this petition button
    Then Only British citizens or UK residents have the right is displayed on the page


  @NoDataNecessary
  Scenario: 3: TST_STYLE_FAIL: I can do a search and the Sign Petition button gets presented - Tester style (low level) - Should FAIL
    When I enter dog into the earch field
    Then I submit the form
    And Stop vets euthanising healthy dogs when options are available to is displayed on the page
    When I click on the Stop vets euthanising healthy dogs when options are available to link
    Then because this is a terrible practice is displayed on the page
    Then I click on the Sign this petition button
    Then Only British citizens or UK residents have the right is displayed on the page


