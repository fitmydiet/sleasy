package uk.automation.sleasy.example.cucumber.stepdefs;


import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import uk.automation.sleasy.example.cucumber.stepdefs.common.CommonSlexampleStep;
import uk.co.jackrabbitventures.sleasy.elementinteraction.SleasyFieldHelper;

public class CommonPageStepDefs extends CommonSlexampleStep {

   //---------------------------------------------------

   @Then("^I submit the form$")
   public void iSubmitTheForm() {

      fld.clickOnSubmit();
   }


   @Then("^(.*) is displayed on the page$")
   public void isDisplayed(String textToFind) {

      fld.textExists(textToFind);
   }

   @When("^I click on the (.*) link$")
   public void clickonthelink(String textToFind) {

      fld.clickOnLink(textToFind);
   }

   @When("^I click on the (.*) button")
   public void clickontheButton(String textToFind) {

      fld.clickOnButton(textToFind);
   }

   @And("^the (.*) button exists$")
   public void theParamButtonExists(String butName) {

      fld.buttonIsEnabled(butName);
   }

   @And("^I enter (.*) into the (.*) field$")
   public void enterTextField(String toEnter, String fldName) {

      fld.fillIn(fldName, toEnter);
   }


   @And("^I enter (.*) into field number (.*)$")
   public void enterTextField(String toEnter, Integer num) {

      fld.setOrdinalForNext(num);
      fld.fillIn(toEnter);
   }


   @And("^I navigate to a valid petition$")
   public void goToPetition() {

      frm.getToAPetitionPage();

   }

   @And("^I click on the (.*) checkbox$")
   public void iClickOnTheBritishCitizenCheckbox(String selText) {

      fld.setCheckTypeOverride(SleasyFieldHelper.XPathCheckType.VISIBLE);
      fld.selectCheckbox(selText);
   }

   @And("^I click on checkbox number (\\d+)$")
   public void iClickOnCheckboxNumber(Integer arg0) throws Throwable {

fld.setOrdinalForNext(arg0);

     // Needs to be taken away from a generic element - should be waiting to be clickable
      fld.setCheckTypeOverride(SleasyFieldHelper.XPathCheckType.PRESENT);

      fld.selectCheckbox();

   }

   @And("^Make sure (.*) appears on the screen$")
   public void makeSureThisIsRightAppearsOnTheScreen(String toCheckFor) throws Throwable {

      fld.textExists(toCheckFor);





   }
}
