package uk.automation.sleasy.example.cucumber.stepdefs;


import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import uk.automation.sleasy.example.cucumber.stepdefs.common.CommonSlexampleStep;

/**
 * Created by Ross Hind on 31/10/2016.
 */
public class SignPetition extends CommonSlexampleStep {

   @Given("^I am on the government petition website home page$")
   public void i_am_on_the_Create_Case_screen() {

      navigateTo("http://petition.parliament.uk");
      fld.textExists("petitions");
      fld.textExists("UK Government and Parliament");
   }

   @When("^I enter valid search criteria into the search box$")
   public void iEnterValidSearchCriteriaIntoTheSearchBox() {
      // Write code here that turns the phrase above into concrete actions
      fld.fillIn("earch","dog");
      fld.clickOnSubmit();

   }

   @And("^a search listing appears$")
   public void aSearchListingAppears() {
      fld.textExists("Stop vets euthanising healthy dogs when options are available to save them");
   }

   @And("^I can sign a petition$")
   public void iCanGetToTheDetailsOfAPetition() throws Throwable {
      fld.clickOnLink( "Stop vets euthanising");
      fld.textExists("Sign");
      fld.clickOnButton("Sign this petition");
      fld.textExists("Only British citizens or UK residents have the right");

   }


}
