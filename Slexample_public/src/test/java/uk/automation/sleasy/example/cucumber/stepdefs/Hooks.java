package uk.automation.sleasy.example.cucumber.stepdefs;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.automation.sleasy.example.cucumber.dataset.SlexampleDataRestorer;
import uk.automation.sleasy.example.cucumber.stepdefs.common.CommonSlexampleStep;


/**
 * A class for the hooks. Note that this class, because it contains hooks,
 * can not its self be extended (a limitation of cucumber).
 * <p>
 * Note that these hooks will run before /after every scenario
 *
 * @author R M Hind.
 */

public class Hooks extends CommonSlexampleStep {

    static final Logger log = LoggerFactory.getLogger(Hooks.class);

    @Before("@NoDataNecessary")
    public void beforeScenarioNoData() {
        beforeHook("dbunitsetup/NoDataNecessary.xml");
    }

    @Before("@CasesForSearchAndMaintain")
    public void beforeScenarioSampleCases() {
        beforeHook("dbunitsetup/CasesForSearchAndMaintain.xml");
    }


    @After()
    public void afterEveryScenario() {
        afterHook();
    }
    
    @Before
    public void beforeAll() {

        // no db connection for this one
      //  beforeAll(new SlexampleDataRestorer());

    }
    
    

}
