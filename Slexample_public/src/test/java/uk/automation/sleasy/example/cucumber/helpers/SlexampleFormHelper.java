package uk.automation.sleasy.example.cucumber.helpers;

import cucumber.api.DataTable;
import gherkin.formatter.model.DataTableRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Ross Hind on 23/09/16.
 */
public class SlexampleFormHelper {

    static final Logger log = LoggerFactory.getLogger(SlexampleFormHelper.class);

    private SlexampleFieldHelper fld;
    
    private DataTable dataTable;

    
    public SlexampleFormHelper(SlexampleFieldHelper fld) {
        this.fld = fld;
    }
    

    private String getData(int i, String theDefault) {
    	String answer = theDefault;

        if (dataTable != null) {
            DataTableRow table = dataTable.getGherkinRows().get(0);
            answer =table.getCells().get(i);
        }
        return answer;
    }


    private String getData(int i) {
        String answer = null;

        if (dataTable != null) {
            DataTableRow table = dataTable.getGherkinRows().get(0);
            answer =table.getCells().get(i);
        } else {
            throw new RuntimeException("you have not provided data in your features file for a step that was expecting it.  Provide a data table in the features file, or a default value in code");
        }
        return answer;
    }


   public void getToAPetitionPage() {

      fld.fillIn("dog");
      fld.clickOnSubmit();
      fld.textExists("Reject calls to add Staffordshire Bull Terriers to the Dangerous Dogs");
      fld.clickOnLink("Reject calls to add Staffordshire Bull Terriers to the Dangerous Dogs");
      fld.textExists("Breed Specific Legislation is not the solution");
     // fld.buttonIsEnabled("Sign this petition");
      fld.clickOnButton("Sign this petition");

   }

}
