package uk.automation.sleasy.example.cucumber.stepdefs.common;


import org.dbunit.dataset.ReplacementDataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.automation.sleasy.example.cucumber.helpers.SlexampleFormHelper;
import uk.automation.sleasy.example.cucumber.helpers.SlexampleFieldHelper;
import uk.co.jackrabbitventures.sleasy.cucumber.CommonStepDef;
import uk.co.jackrabbitventures.sleasy.utils.SuperProps;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static java.time.DayOfWeek.SUNDAY;
import static java.time.temporal.TemporalAdjusters.next;

/**
 * Created by Ross Hind on 23/09/16.
 */
public class CommonSlexampleStep extends CommonStepDef {

   static final Logger log = LoggerFactory.getLogger(CommonSlexampleStep.class);

   // being static is essential for being able to share one instance among all of the concrete stepdefs
   protected static SlexampleFormHelper frm;

   protected static SlexampleFieldHelper fld;

   public CommonSlexampleStep() {
      // set the name of the properties file.  the location is set by super props
      super.superProps = new SuperProps("slexample.props");

   }

   /**
    * initFieldHelper
    * Setting up the two sleasy helpers, field helper and form helper
    */
   @Override
   protected void initHelpers() {

      String screenshotPath = superProps.getProperty("environment.screenshot.path");
      int waitTimeout = Integer.valueOf(superProps.getProperty("wait.timeout.seconds"));
      CommonSlexampleStep.fld = new SlexampleFieldHelper(driver, screenshotPath, waitTimeout);
      CommonSlexampleStep.frm = new SlexampleFormHelper(CommonSlexampleStep.fld);

      //	driver.manage().window().maximize();
   }


   /**
    * Replaces predefined placeholders in the xml with values
    *
    * @param rDataSet
    */
   @Override
   protected void replaceXMLPlaceholders(ReplacementDataSet rDataSet) {

      super.replaceXMLPlaceholders(rDataSet);

      final LocalDate today = LocalDate.now();
      final LocalDate nextSunday = today.with(next(SUNDAY));
      Date nextSundayDate = Date.from(nextSunday.atStartOfDay(ZoneId.systemDefault()).toInstant());

      rDataSet.addReplacementObject("[date_this_sunday]", nextSundayDate);
   }
}
