package uk.automation.sleasy.example.cucumber.dataset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.jackrabbitventures.sleasy.dataset.DatabaseRestorer;

import java.sql.SQLException;

/**
 * Created by Ross Hind on 23/09/16.
 */
public class SlexampleDataRestorer extends DatabaseRestorer {


    public SlexampleDataRestorer() {

        FILENAME = "LatestSlexampleDatasetBackup.xml";
    }

    static final Logger log = LoggerFactory.getLogger(DatabaseRestorer.class);

    @Override
    protected void beforeBackup() throws SQLException {
        // Remove problem causing constraint(s), if any
        try {
            st.executeUpdate("ALTER TABLE relevant_tax_years DROP CONSTRAINT relevant_tax_years_cases_id_fkey");
        } catch (SQLException sqle) {
            log.error("problem before bdd backup", sqle);
        }
    }


    @Override
    protected void afterBackup() throws SQLException {
        //put back the constraint
        try {
            st.executeUpdate("ALTER TABLE relevant_tax_years ADD CONSTRAINT relevant_tax_years_cases_id_fkey FOREIGN KEY (cases_id) "
                    + "REFERENCES cases (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE RESTRICT");
        } catch (SQLException sqle) {
            log.error("problem after bdd backup", sqle);
        }
    }


    @Override
    protected void beforeRestore() throws SQLException {
        // Remove problem causing constraint(s), if any
        try {
            st.executeUpdate("ALTER TABLE relevant_tax_years DROP CONSTRAINT relevant_tax_years_cases_id_fkey");
        } catch (SQLException sqle) {
            log.error("problem before bdd restore", sqle);
        }
    }


    @Override
    protected void afterRestore() throws SQLException {
        //put back the constraint
        try {
            st.executeUpdate("ALTER TABLE relevant_tax_years ADD CONSTRAINT relevant_tax_years_cases_id_fkey FOREIGN KEY (cases_id) "
                    + "REFERENCES cases (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE RESTRICT");
        } catch (SQLException sqle) {
            log.error("problem after bdd restore", sqle);
        }
    }

}
