package uk.automation.sleasy.example.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:reports/cucumber-html-report", "json:reports/cucumber-json-report.json"},
        features = {"src/test/resources/features"}
        //,tags= {"@runthis"}

)
public class CommonJunitRunnerTest {
}
