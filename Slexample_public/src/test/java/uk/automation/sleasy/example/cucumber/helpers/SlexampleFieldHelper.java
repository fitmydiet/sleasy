package uk.automation.sleasy.example.cucumber.helpers;


import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.jackrabbitventures.sleasy.elementinteraction.SleasyFieldHelper;

import java.time.Duration;
import java.util.List;

public class SlexampleFieldHelper extends SleasyFieldHelper {

   static final Logger log = LoggerFactory.getLogger(SleasyFieldHelper.class);

   public SlexampleFieldHelper(WebDriver driver, String screenshotPath, int waitTime) {

      super(driver);
      super.wait = new WebDriverWait(driver, Duration.ofSeconds(waitTime));
      this.screenshotPath = screenshotPath;
      super.tout = waitTime;
   }


   /**
    * Returns the WebElement of the selected Country
    *
    * @param selectionToMake
    */
   private WebElement getAutocompleteDropdownElement(String selectionToMake) {

      try {
         wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".tt-menu.tt-open")));
      } catch (TimeoutException te) {
         String err = "We have waited for the Country drop down menu to appear on the screen but it didn't. ";
         failWithMessage(err);
      }

      List<WebElement> options = driver.findElements(By.cssSelector(".tt-suggestion"));

      for (WebElement option : options) {
         if (selectionToMake.equals(option.getText())) {
            return option;
         }
      }
      return null;
   }

   /**
    * Returns true if the selected country is available in the Country autocompleted dropdown list
    *
    * @param selectionToMake
    */
   public boolean isValueInAutocompleteDropdown(String selectionToMake) {

      if (getAutocompleteDropdownElement(selectionToMake) != null) {
         return true;
      }
      return false;
   }

}
